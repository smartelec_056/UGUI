#include <windows.h>
#include <stdio.h>

LRESULT CALLBACK WindowProcedure(HWND, UINT, WPARAM, LPARAM);
const char szClassName[] = "test";

void setup0(void);
void setup1(void);

int WINAPI WinMain(HINSTANCE hin, HINSTANCE hprev, LPSTR args, int cmd_show)
{
    setup0();
    WNDCLASSEX wincl = {.hInstance = hin, .lpszClassName = szClassName,
            .lpfnWndProc = WindowProcedure, .style = CS_DBLCLKS, .cbSize =
                    sizeof(WNDCLASSEX), .hIcon = LoadIcon(hin, "IDI_ICON"),
            .hIconSm = LoadIcon(hin, "IDI_ICON"), .hCursor = LoadCursor(NULL,
            IDC_ARROW), .lpszMenuName = NULL, .cbClsExtra = 0, .cbWndExtra = 0,
            .hbrBackground = (HBRUSH)(0x0)};

    if(!RegisterClassEx(&wincl))
        return 0;

    HWND hwnd = CreateWindowEx( WS_EX_LEFT, szClassName, "UGUI TEST",
    WS_OVERLAPPEDWINDOW
//    & (~WS_MINIMIZEBOX) & (~WS_MAXIMIZEBOX) & (~WS_SIZEBOX)
        , 300, 300, 300, 200, HWND_DESKTOP, NULL, hin, NULL);

    ShowWindow(hwnd, cmd_show);
    SetTimer(hwnd, 1, 16, NULL);
    setup1();
    MSG messages;
    while(GetMessage(&messages, NULL, 0, 0)) {
        TranslateMessage(&messages);
        DispatchMessage(&messages);
    }
    return messages.wParam;
}

LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT msg, WPARAM wParam,
    LPARAM lParam)
{
    static int wx, wy;
    switch(msg) {
        case WM_SIZE:
            wx = LOWORD(lParam);
            wy = HIWORD(lParam);
            break;
        case WM_PAINT: {
            void Draw(HWND hwnd, HDC* phdc, int wx, int wy);
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hwnd, &ps);
            Draw(hwnd, &hdc, wx, wy);
            EndPaint(hwnd, &ps);
        }
            break;
        case WM_DESTROY:
            PostQuitMessage(0);
            break;
        case WM_CREATE: {
            static HINSTANCE hin;
            hin = (HINSTANCE) GetWindowLong(hwnd, GWL_HINSTANCE);
            hin = hin;
            break;
        }
        case WM_TIMER: {
            void Draw(HWND hwnd, HDC* phdc, int wx, int wy);
            HDC hdc = GetDC(hwnd);
            Draw(hwnd, &hdc, wx, wy);
            ReleaseDC(hwnd, hdc);
            break;
        }
        default:
            return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}
