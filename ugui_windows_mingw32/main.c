#include <windows.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <math.h>
#include "ugui.h"

#define _dbg() do {printf("%d %s\n", __LINE__,__FILE__); fflush(stdout);}while(0)

#define FB_WIDTH 320
#define FB_HEIGHT 480
#define WINDOW_SCALE 1
#define WINDOW_WIDTH (FB_WIDTH * WINDOW_SCALE)
#define WINDOW_HEIGHT (FB_HEIGHT * WINDOW_SCALE)

static struct {
    UG_GUI gui;
    BITMAPINFOHEADER bmih;
    BITMAPINFO bmi;
    HBITMAP hbmp;
    void* buf;
} g;

void init_console(void)
{
    int nRet = 0;
    FILE* fp;
    AllocConsole();
    nRet = _open_osfhandle((long)GetStdHandle(STD_OUTPUT_HANDLE), _O_TEXT);
    fp = _fdopen(nRet, "w");
    *stdout = *fp;
    setvbuf(stdout, NULL, _IONBF, 0);
}

void pset(unsigned short x, unsigned short y, unsigned long color)
{
    if(x < 0 || x >= FB_WIDTH)
        return;
    if(y < 0 || y >= FB_HEIGHT)
        return;
    ((unsigned long*)g.buf)[(FB_HEIGHT - y - 1) * FB_WIDTH + x] = color;
}

void setup0(void)   // called before window show
{
}

void window_1_callback( UG_MESSAGE* msg )
{
        
   if ( msg->type == MSG_TYPE_OBJECT )
   {      
	if ( msg->id == OBJ_TYPE_BUTTON )
	{
		switch( msg->sub_id )
		{
		  case BTN_ID_0:
		  {

			break;
		  }
		case BTN_ID_1: // Show UI info 
		{
		break;
		}
		  
		}
	}
   }
}

/* Window 1 */
UG_WINDOW window_1;
UG_OBJECT obj_buff_wnd_1[10];
UG_BUTTON button1_1;
UG_BUTTON button1_2;
void setup1(void)   // called after window show, before msg loop
{
//    init_console();   // uncomment if necessary
    memset(&g.bmih, 0, sizeof(BITMAPINFOHEADER));
    g.bmih.biSize = sizeof(BITMAPINFOHEADER);
    g.bmih.biBitCount = 32;
    g.bmih.biCompression = BI_RGB;
    g.bmih.biPlanes = 1;
    g.bmih.biWidth = FB_WIDTH;   //m_nBitmapWidth;
    g.bmih.biHeight = FB_HEIGHT;   //m_nBitmapHeight;
    memset(&g.bmi, 0, sizeof(BITMAPINFO));
    g.bmi.bmiHeader = g.bmih;
    g.buf = NULL;

    g.hbmp = CreateDIBSection(NULL, &g.bmi, DIB_RGB_COLORS, &g.buf, NULL, 0);

    UG_Init(&g.gui, (void (*)(UG_S16, UG_S16, UG_COLOR))pset, FB_WIDTH,
    FB_HEIGHT);
    UG_SelectGUI(&g.gui);
    UG_FillScreen(0x0);
    UG_FontSetHSpace(0);
    UG_FontSetVSpace(0);
    UG_SetForecolor(C_CYAN);
    UG_SetBackcolor(0x0);
    UG_FontSelect(&FONT_8X8);
}

void Draw(HWND hwnd, HDC* phdc, int wx, int wy)
{
    static int firstrun = 1;

    // adjust window size
    if((wx != WINDOW_WIDTH || wy != WINDOW_HEIGHT) && firstrun == 1) {
        static int tx = WINDOW_WIDTH, ty = WINDOW_HEIGHT;
        if(wx < WINDOW_WIDTH)
            tx++;
        else if(wx > WINDOW_WIDTH)
            tx--;
        if(wy < WINDOW_HEIGHT)
            ty++;
        else if(wy > WINDOW_HEIGHT)
            ty--;
        MoveWindow(hwnd, 100, 100, tx, ty, TRUE);
        return;
    }
    else
        {firstrun = 0;}
#if 1
	UG_HZFontSelect(&FONT_HANZI_32);
    UG_DrawRoundFrame(3, 3, FB_WIDTH - 3, FB_HEIGHT - 3, 8, C_CYAN);
    UG_SetForecolor(C_CYAN);
    UG_FontSelect(&FONT_12X16);
    UG_PutString((FB_WIDTH - 4*32) / 2, 0, (unsigned char*)"�������");
    char* den = "51.2 MHz";
    UG_SetForecolor(C_RED);
    UG_PutString((FB_WIDTH - strlen(den) * 12) / 2, 32, (unsigned char*)den);

#else
	/* Create Window 1 */
	UG_WindowCreate( &window_1, obj_buff_wnd_1, 10, window_1_callback );
	UG_WindowSetTitleText( &window_1, (unsigned char*)"      GUI @ WINDOWS" );
	UG_WindowSetTitleTextFont( &window_1, &FONT_12X20 );

	/* Create some Buttons */
	UG_ButtonCreate( &window_1, &button1_1, BTN_ID_0, 10, 10, 120, 60 );
	UG_ButtonCreate( &window_1, &button1_2, BTN_ID_1, 10, 80, 120, 130 );
	/* Configure Button 1 */
	UG_ButtonSetFont( &window_1, BTN_ID_0, &FONT_12X20 );
	UG_ButtonSetBackColor( &window_1, BTN_ID_0, C_LIME );
	UG_ButtonSetText( &window_1, BTN_ID_0, (unsigned char*)"About\nGUI" );
	/* Configure Button 2 */
	UG_ButtonSetFont( &window_1, BTN_ID_1, &FONT_12X20 );
    UG_HZFontSelect(&FONT_HANZI_32);
	UG_ButtonSetBackColor( &window_1, BTN_ID_1, C_RED );
	UG_ButtonSetText( &window_1, BTN_ID_1, (unsigned char*)"����AB" );
	
    UG_WindowResize(&window_1,0,0,FB_WIDTH,FB_HEIGHT/2);

	UG_WindowShow( &window_1);
	//Configure Console window
	UG_ConsoleSetArea(0,UG_WindowGetInnerHeight(&window_1)-80,UG_WindowGetXEnd(&window_1),UG_WindowGetYEnd(&window_1));
	UG_ConsoleSetForecolor(C_GREEN_YELLOW);
	UG_ConsoleSetBackcolor(C_BLACK);
    UG_ConsolePutString("This is a test\n");
#endif

    UG_Update();


    SetStretchBltMode(*phdc, STRETCH_HALFTONE);
    StretchDIBits(*phdc, 0, 0, wx, wy, 0, 0, FB_WIDTH, FB_HEIGHT, g.buf, &g.bmi,
    DIB_RGB_COLORS, SRCCOPY);
}
