此GUI主要就是实现了窗口控件、Button控件、CheckBox控件、TextBox控件、Image控件这几个，支持触摸操作。 
  
该GUI的官方地址：http://embeddedlightning.com/ugui/

GITHUB地址： https://github.com/achimdoebler/UGUI


只需要实现三个函数就可以移植了,最主要的是画点函数`_HW_DrawLine`，其他可以不用。
```
UG_RESULT _HW_DrawLine( UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR c );
UG_RESULT _HW_FillFrame( UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR c );
void _HW_DrawPoint(UG_S16 x, UG_S16 y, UG_COLOR c);
```
使用方法
```
    UG_Init(&gui,(void(*)(UG_S16,UG_S16,UG_COLOR))_HW_DrawPoint,LCD_W,LCD_H);//初始化
    UG_DriverRegister(DRIVER_DRAW_LINE,_HW_DrawLine);//注册画线驱动接口
    UG_DriverRegister(DRIVER_FILL_FRAME,_HW_FillFrame);//注册填充接口
```